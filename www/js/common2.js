window.onload = () => {
    let font = new FontFaceObserver('goudy_trajanregular', {
    });

    font.load().then(() => {

        let contentSection = document.getElementById("wr_content");

            const canvasDraw = () => {
                let stageP = document.getElementById("stage-p");
                let stageH1 = document.getElementById("stage-h1");
                let intViewportWidth = window.innerWidth;
                let pSize, h1Size;

                if(intViewportWidth >= 1024){
                    stageP.width = '325';
                    stageH1.width = '780';
                    stageP.height = '60';
                    stageH1.height = '90';
                    pSize = '42px';
                    h1Size = '64px';
                    h1Pos = 60;
                    pPos = 40
                } else if (intViewportWidth >= 768 && intViewportWidth < 1024){
                    stageP.width = '320';
                    stageH1.width = '655';
                    stageP.height = '60';
                    stageH1.height = '80';
                    pSize = '42px';
                    h1Size = '54px';
                    h1Pos = 60;
                    pPos = 40
                } else if (intViewportWidth >= 425 && intViewportWidth < 768){
                    stageP.width = '182';
                    stageH1.width = '350';
                    stageP.height = '40';
                    stageH1.height = '50';
                    pSize = '24px';
                    h1Size = '29px';
                    h1Pos = 40;
                    pPos = 30
                } else {
                    stageP.width = '150';
                    stageH1.width = '230';
                    stageP.height = '30';
                    stageH1.height = '40';
                    pSize = '20px';
                    h1Size = '19px';
                    h1Pos = 30;
                    pPos = 20
                }

                let ctx = stageH1.getContext("2d");
                let ctx2 = stageP.getContext("2d");

                const createText = (element, fontSize, label, height) => {
                    element.font = `${fontSize} goudy_trajanregular`;
                    /* Color gradient */
                    let gradient = element.createLinearGradient(0, 30, 0, 10);
                    gradient.addColorStop("0", "#a68841");
                    gradient.addColorStop("0.5", "#5a4917");
                    gradient.addColorStop("0.6", "#836A28");
                    gradient.addColorStop("1.0", "#E9D07C");
                    /* Text shadow */
                    element.shadowColor = "rgba(0, 0, 0, 0.6)";
                    element.shadowOffsetX = 1; 
                    element.shadowOffsetY = 1; 
                    element.shadowBlur = 0;
                    element.fillStyle = gradient;
                    element.fillText(label, 0, height);
                }

                createText(ctx, h1Size, 'Freestyle consulting', h1Pos);
                createText(ctx2, pSize, 'Coming Soon', pPos);
            }

            const msieversion = () => 
            {
                // let ua = window.navigator.userAgent;
                // let msie = ua.indexOf('MSIE ');
                // let trident = ua.indexOf('Trident/');
                // let edge = ua.indexOf('Edge/');
                // let firefox = ua.indexOf('Firefox');

                // if (msie > 0 || trident > 0 || edge > 0 || firefox > 0) {
                //     contentSection.innerHTML = `
                //         <p>Freestyle consulting</p>
                //         <h1>Coming Soon</h1>
                //     `;
                // }

                // else {
                    contentSection.innerHTML = `
                        <canvas id="stage-h1"></canvas>
                        <canvas id="stage-p"></canvas>
                    `;

                    canvasDraw();
                    window.addEventListener("orientationchange", canvasDraw);
                    window.addEventListener('resize', canvasDraw);
                // }

            }

            msieversion();
        });
    }
