# TerraZ Comming soon page #

We need to have two version of Comming soon page. And use some custom fonts. One of them is Adobe color font.  

## Task issue:  ##
Custom color font Trajan color with gold 3d effect isn't supported by Chrome. Other choosen font is Goudy Trajan Regular doesn't have 3d gold effect by default at all.  

## Solution ##
In index.html we check the client agent. If it's IE or Firefox we just use nice Adobe font as usually. If it's Chrome - we use canvas with JS functions (for creating gold effect and redrawing on window size change or window orientation change).  

Also we need to check first if font is already loaded before JS is run. We use FontFaceObserver library for this.  

In second version (index2.html) we do the same but for all client agents because the font doesn't support 3d gold effect at all.